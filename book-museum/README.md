Install Node Libraries:

```
npm install
```

Create deployment package

```
zip -r deployment-package.zip index.js node_modules
```

Ceate role and get ARN:

```
aws iam create-role --role-name bookmuseum1 --assume-role-policy-document '{"Version": "2012-10-17","Statement": [{ "Effect": "Allow", "Principal": {"Service": "lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}'
```

Attach `AWSLambdaBasicExecutionRole` policy to the above role:

```
aws iam attach-role-policy --role-name bookmuseum1 --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```

Create the Lambda function:

```
aws lambda create-function --function-name bookmuseum --handler index.handler --runtime nodejs14.x --zip-file fileb://deployment-package.zip --role arn:aws:iam::216071967556:role/bookmuseum1
```

